![banner](wp-admin/images/elaser.png)


# TITULO DEL PROYECTO: ELASER RADIÓLOGOS S.A.S.

## índice
1. ⚙️[ Características del Proyecto](#características)
2. 📒[ Contenido del Proyecto](#contenido-del-proyecto)
3. 📱[ Tecnologías](#tecnologías)
4. 🗒️ [ IDE](#ide)
5. 🔧[ Instalación](#instalación)
6. 👀[ Demo](#demo)
7. 👥[ Autor(es)](#autor)
8. 🏫[ Institución Académica](#institución-académica)
9. 📝[Referencias](#referencias)



#### **Características**

- Proyecto realizado con el sistema de gestión de contenido "WordPress".[ver](https://wordpress.com/es/)
- Host utilizado llamado 000webhost. [ver](https://co.000webhost.com/)
- Administrador de bases de datos utilizado: PhpMyAdmin. [ver](https://www.phpmyadmin.net/)



#### **Contenido del Proyecto**

| Archivo| Descripción|
| ----- | ---- |
| [index.html](index.html) | Archivo principal de la página web Elaser Radiólogos. |
| [Js](wp-admin/js) | Archivo Js generado por el sistema de gestión de contenido WordPress |



#### **Tecnologías**

  - ***WORDPRESS***

  > ***Bienvenido. WordPress es un proyecto muy especial para mí. Cada desarrollador y colaborador añade algo único a la mezcla, y juntos creamos algo hermoso de lo que estoy orgulloso de ser parte. Miles de horas han ido a WordPress, y estamos dedicados a hacerlo mejor cada día. Gracias por hacerlo parte de tu mundo.*** - Matt Mullenweg



WordPress es un sistema de gestión de contenidos [CMS](https://www.webempresa.com/blog/que-es-cms-los-mejores-gestores-de-contenido.html) que permite crear y mantener un blog u otro tipo de web.

Con casi 10 años de existencia y más de un millar de [temas](https://es-co.wordpress.org/themes/) (plantillas) disponibles en su web oficial, no es solo un sistema sencillo e intuitivo para crear un blog personal, sino que permite realizar toda clase de web más complejas.

WordPress es un sistema ideal para un sitio web que se actualice periódicamente. Si se escribe contenido con cierta frecuencia, cuando alguien accede al sitio web, puede encontrar todos esos contenidos ordenados cronológicamente (primero los más recientes y por último los más antiguos).

WordPress dispone de un sistema de [plugins](https://es-co.wordpress.org/plugins/), que permiten extender las capacidades de WordPress, de esa forma se consigue un CMS más flexible.

>Uno de los puntos negros WordPress, en cuanto seguridad se refiere, son los formularios a través de los que el usuario introduce sus datos y realiza un comentario. Este plugin desarrollado por los mismos autores de WordPress y es el único (Excepto Hello Dolly) que viene preinstalado en WordPress, chequea de forma automática todos los comentario que se insertan en la web, evitando en gran medida el spam.         **(Corredor Lanas, n.d.)**

Ya hemos respondido la pregunta sobre que es, vamos ahora a explicarte las características básicas de WordPress:

***¿Qué puedo hacer con WordPress?***


En muchas ocasiones se asocia WordPress con una herramienta que solo sirve para hacer blogs. Esto no es correcto: con WordPress podemos crear un blog y mucho más: webs empresariales, tiendas online, periódico digital, central de reservas, etc. A continuación vamos a ver algunas de las cosas que podemos crear con este gestor de contenidos.

- Blog
- Web corparativa
- Tienda online
- Y mucho mas...

por lo que **WordPress** es una excelente herramienta para empezar a crear tu propio blog o pagina web 



- ***PHP-MY-ADMIN***

PHPMyAdmin es un software de código abierto, diseñado para manejar la administración y gestión de bases de datos MySQL a través de una interfaz gráfica de usuario. Escrito en PHP, se ha convertido en una de las más populares herramientas basadas en web de gestión de Base de datos, viene con una documentación detallada y está siendo apoyado por un gran multi-idioma de la comunidad.

>PHP es un lenguaje de script que se ejecuta del lado del servidor, el código PHP se incluye en una pagina HTML normal. Por lo tanto, se pude comparar con otros lenguajes de script que se ejecutan según el mismo principio. ASP (Active Server Pages) JSP (Java Server Pages) o PL/SQL Server Pages PSP    ****(Heurtel, 2016)****


>PHP es software libre licenciado bajo la PHP license, una licencia incompatible con la GNU General Public License (GPL) debido a las restricciones en los términos de uso de PHP   ****(Arias, n.d.)****

***¿Qué nos ofrece PhpMyAdmin?***
Esta herramienta es muy completa y nos ofrece una gran cantidad de usos y características, algunas de ellas son:

1. Esta aplicación nos permitirá realizar las operaciones básicas en base de datos MySQL, como son: crear y eliminar bases de datos, crear, eliminar y alterar tablas, borrar, editar y añadir campos, ejecutar sentencias SQL, administrar claves de campos, administrar privilegios y exportar datos en varios formatos. La función de exportar datos se emplea muchas veces para realizar backups de la base de datos y poder restaurar esta copia de seguridad en el futuro a través de phpMyAdmin mediante la opción “importar”.
2. phpMyAdmin es el administrador de bases de datos por defecto en muchos paneles de control comerciales como son Lampp, Wampp o Xampp.
3. Los usuarios no deberían tener problemas a la hora de manejar esta herramienta, ya que es fácil de usar.
4. Otra de las funciones más importantes que nos ofrece es que permite optimizar y reparar tablas, las cuales son dos tareas de mantenimiento fundamentales.
5. Nos da la posibilidad de realizar búsquedas en la base de datos, además de poder escribir nuestras propias consultas SQL de manera directa y ejecutarlas.
6. Esta herramienta también es de gran ayuda para desarrolladores de aplicaciones que empleen MySQL, ya que permite depurar consultas y hacer test de forma rápida y sencilla.


[![pipeline status](https://gitlab.com/FelipeM09/elaser-radiologos/badges/master/pipeline.svg)](https://gitlab.com/FelipeM09/elaser-radiologos/-/commits/master)

[![WordPress Plugin Active Installs](https://img.shields.io/wordpress/plugin/installs/akismet?label=Plugin%20Akismet&logo=WordPress)](https://es-co.wordpress.org/plugins/akismet/)

[![WordPress Theme Last Updated](https://img.shields.io/wordpress/theme/last-updated/ashe?label=Theme%20Ashe&logo=WordPress)](https://es-co.wordpress.org/themes/ashe/)

[![WordPress Plugin Active Installs](https://img.shields.io/wordpress/plugin/installs/booking?label=Plugin%20Booking&logo=WordPress)](https://es-co.wordpress.org/plugins/booking/)



#### **IDE**

Es importante aclarar que en este proyecto no se utilizo ningun IDE (pero si se hicieron algunas modificaciones en los archivos HTML del proyecto, pero todo ello con el CMS Wordpress) porque la pagina web se desarrollo con un sistema de control de contenido (CMS) muy reconocido a nivel mundial llamado WordPress [Cómo usarlo](https://www.youtube.com/watch?v=8A3h31xmqD8&ab_channel=VincenzoMicale)



- ***¿Que es WordPress?***

   En esencia, WordPress es la manera más simple y popular de crear su propio sitio web o blog. De hecho, WordPress tiene más del 40.0% de todos los sitios web en Internet. Sí, es probable que WordPress funcione con más de uno de cada cuatro sitios web que visita.

   En un nivel un poco más técnico, WordPress es un sistema de gestión de contenido de código abierto con [licencia GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0-faq.es.html), lo que significa que cualquier persona puede usar o modificar el software de WordPress de forma gratuita. Un sistema de administración de contenido es básicamente una herramienta que facilita la administración de aspectos importantes de su sitio web, como el contenido, sin necesidad de saber nada sobre programación.

   El resultado final es que WordPress hace que la construcción de un sitio web sea accesible para cualquier persona, incluso para personas que no son desarrolladores.

![img](https://s.w.org/images/home/screen-themes.png?4)



#### **Instalación WordPress en local**

Para la instalación local debemos instalar un host local, en este caso usaremos XAMPP.

- Ingresamos a la página oficial de XAMPP y realizamos la instalación.[ir a XAMPP.](https://www.apachefriends.org/es/index.html).
- Ejecutamos el archivo .exe que nos deja la descarga y procedemos a la instalación.
- Terminada la instalación abriremos el XAMPP y activamos los modulos "Apache" y "MySQL" y minimizamos XAMPP.
-Entramos a la carpeta donde se instaló XAMPP y creamos una carpeta con el nombre de nuestro proyecto.

Ahora instalaremos WordPress en XAMPP.

- Entramos a la sección de descargas de la página oficial de WordPress para descargarlo. [Clic Aquí.](https://es-co.wordpress.org/download/#download-install).
- Descargarmos WordPress en su version actual.
- Descomprimimos el archivo .zip que nos dejó la descarga.
- Abrimos la carpeta que nos dejó y copiamos  o cortamos todos los archivos y carpetas que existen en esa carpeta.
- Abrimos la carpeta que creamos en la carpeta de origen de XAMPP y pegamos los archivos copiados.

Ya tenemos instalado Wordpress en XAMPP, ahora crearemos una base de datos para nuestro proyecto.

- Entramos a cualquier navegador y en la barra de direcciones escrbimos "localhost/" Y damos enter.
- Luego, damos clic en phpmyadmin
- Una vez dentro de phpmyadmin seleccionamos "bases de datos" creamos la base de datos, ponemos nombre a nuestra base.
- Ahora ingresamos al WordPress desde nuestro localhost, escribimos en la barra de direcciones "localhost/nombre_carpeta" y creamos nuestro proyecto en WordPress.

Completamos los formularios para empezar nuestro proyecto y finalizamos la instalación local.




#### **Demo**
- Se puede visualizar la versión del demo (un prototipo de la pagina web) [Aquí.](https://elaserradiologos.000webhostapp.com/quienes-somos)



#### **Autor(es)**
Proyecto desarrollado por: **Manuel Felipe Mora Espitia:** [<manuelfelipeme@ufps.edu.co>](manuelfelipeme@ufps.edu.co), **Brayan Andrés Roa López:** [brayanandresrl@ufps.edu.co](brayanandresrl@ufps.edu.co)



#### **Institución Académica**

Proyecto realizado por el programa de [**Ingeniería de Sistemas**](https://ingsistemas.cloud.ufps.edu.co/) de la [**Universidad Francisco de Paula Santander**](https://ww2.ufps.edu.co/)


#### **Referencias**

Corredor Lanas, A., n.d. Wordpress profesional edición 2017. [ver](https://books.google.com.co/books?id=go6fDwAAQBAJ&pg=PA356&dq=wordpress&hl=es&sa=X&ved=2ahUKEwjbjJDoo5zwAhX_QjABHeRZD5YQ6AEwAXoECAAQAg#v=onepage&q=wordpress&f=false)

Heurtel, O. (2016). PHP 7. [Barcelona]: ENI. [ver](https://books.google.com.co/books?hl=es&lr=&id=EJ1t1GsMHbIC&oi=fnd&pg=PA1&dq=php&ots=aO4K8S_K0W&sig=bfEFIg2N17EiE_BDIBGwKhz8tts&redir_esc=y#v=onepage&q=php&f=false)

Arias, M. Aprende programación con PHP y MySQL. [ver](https://books.google.com.co/books?hl=es&lr=&id=mP00DgAAQBAJ&oi=fnd&pg=PA13&dq=mysql&ots=DMLnnB5KqT&sig=sJ111xBIzsOz48rhLRs1VfdAZ8s&redir_esc=y#v=onepage&q=mysql&f=false)
